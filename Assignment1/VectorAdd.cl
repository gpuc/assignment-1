__kernel void VecAdd(__global const int* a, __global const int* b, __global int* c, int numElements)
{
	// get global id
	int gid = get_global_id(0);

	// check if global id is within bounds
	if (gid < numElements)
	{
		// sum elements of both source vectors and write result into target
		c[gid] = a[gid] + b[numElements - gid - 1];
	}
}
