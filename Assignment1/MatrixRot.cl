
// Rotate the matrix CLOCKWISE

//naive implementation: move the elements of the matrix directly to their destinations
//this will cause unaligned memory accessed which - as we will see - should be avoided on the GPU

__kernel void MatrixRotNaive(__global const float* M, __global float* MR, uint SizeX, uint SizeY)
{
	// load global ids
	int2 gid;
	gid.x = get_global_id(0);
	gid.y = get_global_id(1);
	
	// check if local ids are within matrix bounds
	if (gid.x < SizeX && gid.y < SizeY)
	{
		// rotate single matrix entry and write it back into result matrix
		MR[gid.x * SizeY + (SizeY - gid.y - 1)] = M[gid.y * SizeX + gid.x];
	}
}

//this kernel does the same thing, however, the local memory is used to
//transform a small chunk of the matrix locally
//then write it back after synchronization in a coalesced access pattern

__kernel void MatrixRotOptimized(__global const float* M, __global float* MR, uint SizeX, uint SizeY,
							__local float* block)
{
	// load global ids
	int2 gid;
	gid.x = get_global_id(0);
	gid.y = get_global_id(1);

	// load local ids
	int2 lid;
	lid.x = get_local_id(0);
	lid.y = get_local_id(1);

	// load local sizes
	int2 ls;
	ls.x = get_local_size(0);
	ls.y = get_local_size(1);

	// index within block array
	int blockIndex = lid.y * ls.x + lid.x;

	// load tile into local memory if indices are valid
	if (gid.x < SizeX && gid.y < SizeY && lid.x < ls.x && lid.y < ls.y)
	{
		block[blockIndex] = M[gid.y * SizeX + gid.x];
	}

	// barrier to wait until all local threads are finished
	barrier(CLK_LOCAL_MEM_FENCE);

	// calculate global block position
	int2 blockPos;
	blockPos.x = gid.x - lid.x;
	blockPos.y = gid.y - lid.y;

	// calculate rotated block position
	int2 rotatedBlockPos;
	rotatedBlockPos.x = SizeY - blockPos.y - ls.y;
	rotatedBlockPos.y = blockPos.x;

	int2 resultPos;
	int2 blockReadPos;

	// quadratic block size
	if (ls.x == ls.y)
	{
		// calculate target postion within rotated matrix
		resultPos.x = rotatedBlockPos.x + lid.x;
		resultPos.y = rotatedBlockPos.y + lid.y;

		// calculate rotated position within block
		blockReadPos.x = lid.y;
		blockReadPos.y = ls.y - lid.x - 1;
	}
	// non-quadratic block size
	else
	{
		// calculate target postion within rotated matrix
		int2 innerBlockWriteOffset;
		innerBlockWriteOffset.x = blockIndex % ls.y;
		innerBlockWriteOffset.y = blockIndex / ls.y;

		resultPos.x = rotatedBlockPos.x + innerBlockWriteOffset.x;
		resultPos.y = rotatedBlockPos.y + innerBlockWriteOffset.y;

		// calculate rotated position within block
		blockReadPos.x = innerBlockWriteOffset.y;
		blockReadPos.y = ls.y - innerBlockWriteOffset.x - 1;
	}

	// write back results and read block column wise doing so if indices are valid
	if (resultPos.y < SizeX && resultPos.x < SizeY && blockReadPos.y < ls.y && blockReadPos.x < ls.x)
	{
		MR[resultPos.y * SizeY + resultPos.x] = block[blockReadPos.y * ls.x + blockReadPos.x];
	}
}
